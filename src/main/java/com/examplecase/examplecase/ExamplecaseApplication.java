package com.examplecase.examplecase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamplecaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamplecaseApplication.class, args);
	}

}
